package eu.ase.main;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import eu.ase.classes.Matrice;

public class Main {
	public static void main(String[] args) {
		//citire de la tastatura
		//scriere in fisier (text)
		//citire din fisier (text)
		//scriere in fisier (binar)
		//citire din fisier (binar)
		int line = 0;
		int col = 0;
		int[][]m=null;
		//fisiere text
		
		//citire de la tastatura
		InputStreamReader is=new InputStreamReader(System.in);
		BufferedReader buf=new BufferedReader(is);
		
			try {
				System.out.print("Lines : ");
				line=Integer.parseInt(buf.readLine());
				System.out.print("Columns : ");
				col=Integer.parseInt(buf.readLine());
				m=new int[line][col];
				for(int i=0;i<line;i++)
				{
					for(int j=0;j<col;j++)
					{
						
						m[i][j]=Integer.parseInt(buf.readLine());
					}
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			Matrice matrix=new Matrice(line, col, m);
			System.out.println(matrix);
			
			//scriere in fisier
			try {
				FileOutputStream fis2=new FileOutputStream("matrix.txt");
				OutputStreamWriter osw=new OutputStreamWriter(fis2);
				osw.write(Integer.toString(matrix.getLines()));
				osw.write(System.lineSeparator());
				osw.write(Integer.toString(matrix.getColumns()));
				osw.write(System.lineSeparator());
				for(int i=0;i<matrix.getLines();i++)
				{
					for(int j=0;j<matrix.getColumns();j++)
					{
						osw.write(Integer.toString(matrix.data[i][j]));
						osw.write(System.lineSeparator());
					}
					
				}
				osw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		//citire in fisier
			
			int line2=0;
			int col2=0;
			int[][]m2=null;
		try {
			FileInputStream fis=new FileInputStream("matrix.txt");
			InputStreamReader isf=new InputStreamReader(fis);
			BufferedReader buf2=new BufferedReader(isf);	
			line2=Integer.parseInt(buf2.readLine());
			col2=Integer.parseInt(buf2.readLine());
			m2=new int[line2][col2];
			for(int i=0;i<line2;i++)
			{
				for(int j=0;j<col2;j++)
				{
					
					m2[i][j]=Integer.parseInt(buf2.readLine());
				}
			}
			buf2.close();
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		} catch (NumberFormatException e) {

			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Matrice matrix2=new Matrice(line2, col2, m2);
		System.out.println("--From file--\n");
		System.out.println(matrix2);
		
		//Fisiere binare
		
		//scriere in fisiere binare
		try(FileOutputStream fos =new FileOutputStream("Matrice.bin");
				DataOutputStream data= new DataOutputStream(fos))
		{
			data.writeInt(matrix2.getLines());
			data.writeInt(matrix2.getColumns());
			for(int i=0;i<matrix2.getLines();i++)
			{
				for(int j=0;j<matrix2.getColumns();j++)
				{
					data.writeInt(matrix2.data[i][j]);
				}
			}
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		try(FileInputStream fis= new FileInputStream("Matrice.bin");
				DataInputStream data=new DataInputStream(fis))
		{
			int line3=data.readInt();
			int col3=data.readInt();
			int[][]m3=new int[line3][col3];
			for(int i=0;i<line3;i++)
			{
				for(int j=0;j<col3;j++)
				{
					m3[i][j]=data.readInt();
				}
			}
			Matrice matrixB=new Matrice(line3, col3, m3);
			System.out.println("---Read from binary file----");
			System.out.println(matrixB);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
