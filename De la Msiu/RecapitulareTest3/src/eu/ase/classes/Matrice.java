package eu.ase.classes;

public class Matrice {
private int lines;
private int columns;
public int[][] data;
public int getColumns() {
	return columns;
}
public void setColumns(int columns) {
	this.columns = columns;
}
public int getLines() {
	return lines;
}
public void setLines(int lines) {
	this.lines = lines;
}
public Matrice(int lines,int columns,int[][] matrice)
{
	this.lines=lines;
	this.columns=columns;
	this.data=new int[this.lines][this.columns];
	for(int i=0;i<this.lines;i++)
	{
		for(int j=0;j<this.columns;j++)
		{
			this.data[i][j]=matrice[i][j];
		}
	}
}
@Override
public String toString() {
	StringBuilder sb=new StringBuilder();
	sb.append("  Lines : ");
	sb.append(this.lines);
	sb.append("\nColumns : ");
	sb.append(this.columns);
	sb.append("\n\n");
	for(int i=0;i<this.lines;i++)
	{
		for(int j=0;j<this.columns;j++)
		{
			sb.append(" ");
			sb.append(this.data[i][j]);
		}
		sb.append("\n");
	}
	return sb.toString();
}


}
