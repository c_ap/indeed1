package eu.ase.classes;

public abstract class Persoana implements Cloneable {
	private String name;
	private int age;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Persoana(String Name,int age)
	{
		this.name=Name;
		this.age=age;
	}
	
	public abstract String getUsername();
	@Override
	public Object clone() throws CloneNotSupportedException {
		Persoana copy=(Persoana)super.clone();
		copy.name=this.name;
		copy.age=this.age;
		return copy;

	}
	
	
}
