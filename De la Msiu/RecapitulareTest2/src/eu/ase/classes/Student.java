package eu.ase.classes;

import eu.ase.interfaces.Greeting;

public class Student extends Persoana implements Cloneable,Greeting{

	private int noGrades;
	private int[] grades;
	
	public Student(String Name, int age, int noGrades,int[] grades) {
		super(Name, age);
		this.noGrades=noGrades;
		this.grades=new int[this.noGrades];
		for(int i=0;i<this.noGrades;i++)
			this.grades[i]=grades[i];
	}
	
	public int getNoGrades() {
		return noGrades;
	}

	public void setNoGrades(int noGrades) {
		this.noGrades = noGrades;
	}

	public int[] getGrades() {
		return grades;
	}

	public void setGrades(int[] grades) {
		this.grades = grades;
	}

	

	@Override
	public String getUsername() {
		StringBuilder sb=new StringBuilder();
		sb.append(this.getName());
		sb.append(this.getAge());
		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("Name: ");
		sb.append(this.getName());
		sb.append("\nAge: ");
		sb.append(this.getAge());
		sb.append("\nNumber of grades: ");
		sb.append(this.noGrades);
		sb.append("\nGrades: ");
		for(int i=0;i<this.noGrades;i++)
		{
			sb.append(" ");
			sb.append(this.grades[i]);
		}
		
		sb.append("\nUsername: ");
		sb.append(this.getUsername());
		return sb.toString();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Student copy=(Student)super.clone();
		copy.noGrades=this.noGrades;
		copy.grades=new int[copy.noGrades];
		for(int i=0;i<copy.noGrades;i++)
			copy.grades[i]=this.grades[i];
		return copy;
		
	}

	@Override
	public void sayHello() {
		System.out.println("\nHi! My username is "+ this.getUsername());
		
	}
	
	
}
