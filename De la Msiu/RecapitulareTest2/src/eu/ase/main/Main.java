package eu.ase.main;

import eu.ase.classes.Student;

public class Main {
	//inheritance
	//overriding toString(), Equals() and clone()
	//abstract class
	//derivated class
	//interface***** and implementing Clonable in both abstract and derived class ***
	//         *****as well as overriding the clone() method for both of them****
	//getters and setters
	public static void main(String[] args) {
		int[] note= {7,8,6};
		Student s1=new Student("Mihai", 21, 3, note);
		
		System.out.println(s1);
		
		try {
			Student s2=(Student)s1.clone();
			System.out.println("----------------");
			System.out.println(s2);
			s2.sayHello();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
