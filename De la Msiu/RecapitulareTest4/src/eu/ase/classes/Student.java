package eu.ase.classes;

import java.io.Serializable;

public class Student implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -936369341679715528L;
	private String name;
	private int age;
	public Student(String name,int age)
	{
		this.setName(name);
		this.setAge(age);
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("Name : ");
		sb.append(this.name);
		sb.append("\nAge: ");
		sb.append(this.age);
		return sb.toString();
	}

}
