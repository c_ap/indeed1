package eu.ase.main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import eu.ase.classes.Student;

public class Main {
//serialization and deseralization
	public static void main(String[] args) {
		Student s1=new Student("Irina", 21);
		System.out.println("---------Serialization----------");
		System.out.println(s1);
		try(FileOutputStream fos=new FileOutputStream("Student.cry"))
		{
			ObjectOutputStream oos= new ObjectOutputStream(fos);
			oos.writeObject(s1);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		System.out.println("-------Deserialization----------");
		try(FileInputStream fis = new FileInputStream("Student.cry"))
		{
			ObjectInputStream ois=new ObjectInputStream(fis);
			Student s2=(Student)ois.readObject();
			System.out.println(s2);
		}
		catch(IOException | ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}

}
