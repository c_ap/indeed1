package eu.ase.main;

import eu.ase.classes.Operation;

public class Main {
	//lambda expressions
	public static void main(String[] args) {
	
	Operation add;
	add=(double a, double b) ->(a+b);
	System.out.println(add.operation(5, 7));
	
	
	Operation substract=(a,b)->(a-b);
	System.out.println(substract.operation(5, 7));
	
	Operation multiply=(a,b)->a*b;
	System.out.println(multiply.operation(5, 7));
	
	Operation divide=(a,b)->(a/b);
	System.out.println(divide.operation(5, 7));
	System.out.println(divide.operation(0, 7));
	System.out.println(divide.operation(5, 0));
	
	Operation print=(a,b)->
	{
		System.out.println(a+" + "+b+" = "+(a+b));
		return (a+b);
	};
	
	print.operation(5, 9);
	Operation pet=(a,b)->{
		System.out.println("\nIrina esti "+a+" la un "+b+". <3");
		
		return 0;
	};
	
	pet.operation(1, 1_000_000);
		pet.whatever();
	
}
}