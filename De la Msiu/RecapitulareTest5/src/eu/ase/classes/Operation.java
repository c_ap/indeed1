package eu.ase.classes;
@FunctionalInterface
public interface Operation  {
	public double operation(double a,double b);
	default public void whatever()
	{
		System.out.println("Default whatever");
	}
}
