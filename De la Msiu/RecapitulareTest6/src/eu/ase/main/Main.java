package eu.ase.main;


import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.stream.Collectors;

public class Main {
	//parallel Streams
	//filter(lambda expression) -returneaza bool 
	//map(lambda expression) - modifici chestii
	//vector
	public static void main(String[] args) {
		
		List<String> list=Arrays.asList("Irina","este","o","fata","desteapta");
		
		long no=list.stream().filter(a ->(a.contains("I"))).count();
		System.out.println("Words that contains 'I' : "+no);
		
		long no2=list.stream().filter(a->(a.length()>0)).count();
		System.out.println("Words that contains at least one character : "+no2);
		
		List<String> list2=list.stream().filter(s->(s.contains("s"))).collect(Collectors.toList());
		System.out.println(list2);
		
		List<Integer> list3=Arrays.asList(1,2,3,4,5,6);
		System.out.println(list3);
		
		List<Integer> list4=list3.stream().map(x->(x*x)).collect(Collectors.toList());
		System.out.println(list4);

		List<String> list5=list.stream().map(s->(s.toUpperCase())).collect(Collectors.toList());
		System.out.println(list);
		System.out.println(list5);
		
		
		Vector<Integer>vector1=new Vector<Integer>();
		vector1.add(0,10);
		vector1.add(1,20);
		vector1.add(2,30);
		vector1.forEach(System.out::println);
		Vector<Integer>vector2=new Vector<Integer>();
		Vector<Integer> clone =(Vector<Integer>) vector1.clone();
		vector2=clone;
			System.out.println(vector2);
		Random rand=new Random();
		List<Integer> randList=Arrays.asList(1,2,3,5);
		System.out.println(randList);
		randList=randList.stream().map(i->(i*rand.nextInt(60))).collect(Collectors.toList());
		System.out.println(randList);
	}
}
