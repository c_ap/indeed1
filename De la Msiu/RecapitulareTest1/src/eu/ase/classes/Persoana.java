package eu.ase.classes;

public class Persoana {
	private String name;
	private int age;
	private int noCountries;
	private String[] visitedCountries;
	public Persoana(String Name,int age,int noCountries, String[] visitedCountries)
	{
		this.name=Name;
		this.age=age;
		this.noCountries=noCountries;
		this.visitedCountries=new String[this.noCountries];
		this.setVisitedCountries(visitedCountries);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String[] getVisitedCountries() {
		String[] copy=new String[this.noCountries];
		for(int i=0;i<this.noCountries;i++)
		System.arraycopy(this.visitedCountries,i, copy, i, 1);
		
		return copy;
	}
	public void setVisitedCountries(String[] visitedCountries) {
		for(int i=0;i<this.noCountries;i++)
			System.arraycopy(visitedCountries,i, this.visitedCountries, i, 1);
	}
	public int getNoCountries() {
		return noCountries;
	}
	public void setNoCountries(int noCountries) {
		this.noCountries = noCountries;
	}
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("Name: ");
		sb.append(this.name);
		sb.append("\nage: ");
		sb.append(this.age);
		sb.append("\nNumber of countries visited : ");
		sb.append(this.noCountries);
		sb.append("\n");
		for(int i=0;i<this.noCountries;i++)
		{
			sb.append(" ");
			sb.append(this.visitedCountries[i]);
		}
		return sb.toString();
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		Persoana copy=new Persoana(this.name, this.age, 
				this.noCountries, this.visitedCountries);
		return copy;
			
	}
	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof Persoana))
			return false;
		else
		{
			Persoana p=(Persoana)obj;
		boolean a=true;
		for(int i=0;i<this.noCountries;i++)
			if((this.visitedCountries[i].equals(p.visitedCountries[i]))==false)
			{
				a=false;
				return a;
			}
		return a && this.name.equals(p.name) && this.age==p.age&& this.noCountries==p.noCountries;
		}
	}
	

	
}
