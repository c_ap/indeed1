package eu.ase.main;

import eu.ase.classes.Persoana;

public class Main {
	//a class with default and parametrized constructors
	//getters and setters
	//overriding toString(), Equals(), clone() 
	public static void main(String[] args) {
		String[] tari= {"Romania","Moldova"};
		Persoana p1=new Persoana("Gigel",10,2,tari);
		
		tari[0]="Italia";
		Persoana p3=new Persoana("Gigel",10,2,tari);
		System.out.println(p1);
		System.out.println("---------");
		try {
			Persoana p2=(Persoana)p3.clone();
			System.out.println(p2);
			System.out.println("---------");
			p2.setAge(99);
			System.out.println(p3);
			System.out.println(p1.equals(p2));
			System.out.println(p2.equals(p3));
			Persoana p4=(Persoana)p3.clone();
			System.out.println(p3.equals(p4));
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
