package eu.ase.classes;

import java.util.Random;

public class SyncThread  extends Thread{
	
	public Random rand=new Random();
	public static Object locked=new Object();
	public SyncThread(String Name) {
		this.setName(Name);
	}
	@Override
	public void run() 
	{
		synchronized (locked) 
		{
			super.run();
				try {
					Thread.sleep(rand.nextInt(30)*200);
					System.out.println("Sync Thread "+this.getName()+" says Hi!");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		}
	}
	
}
