package eu.ase.classes;

import java.util.Random;

public class UnsyncThreadR implements Runnable{
	public Random rand=new Random();
	public String Name;
	public UnsyncThreadR(String Name) {
		this.Name=Name;
	}
	@Override
	public void run() {
		
		try {
			Thread.sleep(rand.nextInt(30)*200);
			System.out.println("Unsync Thread (Runnable) "+this.Name+" says Hi!");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
