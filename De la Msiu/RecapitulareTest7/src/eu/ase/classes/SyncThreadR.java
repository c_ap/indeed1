package eu.ase.classes;

import java.util.Random;

public class SyncThreadR implements Runnable {
	public Random rand=new Random();
	public String Name;
	public static Object locked=new Object();
	public SyncThreadR(String Name) {
		this.Name=Name;
	}
	@Override
	public void run()
	{
	
		synchronized (locked) 
		{
			try {
				Thread.sleep(rand.nextInt(30)*200);
				System.out.println("Sync Thread (Runnable) "+this.Name+" says Hi!");
				} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
		
	}
	
}
