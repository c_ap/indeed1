package eu.ase.main;

import eu.ase.classes.SyncThread;
import eu.ase.classes.SyncThreadR;
import eu.ase.classes.UnsyncThread;
import eu.ase.classes.UnsyncThreadR;

public class Main {

	public static void main(String[] args) {
		System.out.println("----Unsynchronized Threads Runnable interface mode------");
		UnsyncThreadR UTR1=new UnsyncThreadR("UTR1");
		UnsyncThreadR UTR2=new UnsyncThreadR("UTR2");
		Thread t1=new Thread(UTR1);
		Thread t2=new Thread(UTR2);
		t1.start();
		t2.start();
		System.out.println("----Synchronized Threads Runnable interface mode------");
		SyncThreadR STR3=new SyncThreadR("STR3");
		SyncThreadR STR4=new SyncThreadR("STR4");
		Thread t3=new Thread(STR3);
		Thread t4=new Thread(STR4);
		t3.start();
		t4.start();
		System.out.println("----Unsynchronized Threads  interface mode------");
		UnsyncThread STR5=new UnsyncThread("STR5");
		UnsyncThread STR6=new UnsyncThread("STR6");
		STR5.start();
		STR6.start();
		System.out.println("----Synchronized Threads Runnable interface mode------");
		SyncThread STR7=new SyncThread("STR7");
		SyncThread STR8=new SyncThread("STR8");
		STR7.start();
		STR8.start();
	}

}
