package eu.ase;

import java.io.Serializable;

public class TitluCalatorieMetropolitan extends TitluCalatorie implements Cloneable, Serializable, Comparable<TitluCalatorieMetropolitan>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5236642733094971393L;
	private String denumireOperatorMetropolitan;
	
	//CONSTRUCTOR
	public TitluCalatorieMetropolitan(int id, String denumire, float idLinie, String dataStart, String dataStop, String denumireOperatorMetropolitan) throws Exception {
		super(id, denumire,idLinie,dataStart,dataStop);
		if(idLinie<0) {
			throw new Exception();
		}
		this.denumireOperatorMetropolitan=denumireOperatorMetropolitan;
	}

	//GETTER SI SETTER
	public String getDenumireOperatorMetropolitan() {
		return denumireOperatorMetropolitan;
	}

	public void setDenumireOperatorMetropolitan(String denumireOperatorMetropolitan) {
		this.denumireOperatorMetropolitan = denumireOperatorMetropolitan;
	}

	@Override
	public String getIdZonaAbs() {
		
		return new String(this.getIdLinie()+this.denumireOperatorMetropolitan);
	}

	//CLONE
	@Override
	protected Object clone() throws CloneNotSupportedException {
		
		TitluCalatorieMetropolitan tm = (TitluCalatorieMetropolitan)super.clone();
		
		try {
			return new TitluCalatorieMetropolitan(this.getId(), this.getDenumire(),
					this.getIdLinie(), this.getDataStart(), this.getDataStop(), this.denumireOperatorMetropolitan);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}

	//EQUALS
	@Override
	public boolean equals(Object obj) {

		if(obj==null || !(obj instanceof TitluCalatorieMetropolitan)) {
			return false;
		}
		
		TitluCalatorieMetropolitan tm = (TitluCalatorieMetropolitan)obj;
		
		
		return tm.getId()==this.getId() && tm.getDenumire().equals(this.getDenumire()) &&
				tm.getIdLinie()==this.getIdLinie() && tm.getDataStart().equals(this.getDataStart()) &&
				tm.getDataStop().equals(this.getDataStop()) && tm.denumireOperatorMetropolitan.equals(this.denumireOperatorMetropolitan);
	}

	//HASHCODE
	@Override
	public int hashCode() {
		return (int)(31*this.getId()+31*this.getIdLinie()+31*this.getDenumire().length()+31*this.denumireOperatorMetropolitan.length());
	}

	//TOSTRING
	@Override
	public String toString() {
		return new String("\n\nID: "+this.getId()+"\nDenumire: "+this.getDenumire()+
				"\nID Linie: "+this.getIdLinie()+"\nData Start: "+this.getDataStart()+
				"\nData Stop: "+this.getDataStop()+"\nID Zona + Denumire Operator Metropolitan: "+this.getIdZonaAbs());
		
	}

	@Override
	public int compareTo(TitluCalatorieMetropolitan o) {
		// TODO Auto-generated method stub
		return this.getDataStop().compareTo(o.getDataStop());
	}
	
	
	
	
	
	
	

}
