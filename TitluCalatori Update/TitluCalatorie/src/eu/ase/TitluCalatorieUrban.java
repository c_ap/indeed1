package eu.ase;

import java.io.Serializable;

public class TitluCalatorieUrban extends TitluCalatorie implements Cloneable, Serializable, Comparable<TitluCalatorieUrban>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5027358724210038345L;
	private String denumireOperatorUrban;
	
	//CONSTRUCTOR
	public TitluCalatorieUrban(int id, String denumire, float idLinie, String dataStart, String dataStop, String DenumireOperatorUrban) throws Exception {
		super(id, denumire,idLinie,dataStart,dataStop);
		if(idLinie<0) {
			throw new Exception();
		}
		this.denumireOperatorUrban=DenumireOperatorUrban;
	}

	//GETTER SI SETTER
	public String getDenumireOperatorUrban() {
		return denumireOperatorUrban;
	}

	public void setDenumireOperatorUrban(String denumireOperatorUrban) {
		this.denumireOperatorUrban = denumireOperatorUrban;
	}

	@Override
	public String getIdZonaAbs() {
		
		return new String(this.getIdLinie()+this.denumireOperatorUrban);
	}

	//CLONE
	@Override
	protected Object clone() throws CloneNotSupportedException {
		
		TitluCalatorieUrban tu =(TitluCalatorieUrban)super.clone();
		
		try {
			return new TitluCalatorieUrban(tu.getId(), tu.getDenumire(), tu.getIdLinie(), tu.getDataStart(), tu.getDataStop(), tu.denumireOperatorUrban);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	
	//EQUALS
	@Override
	public boolean equals(Object obj) {
		
		if(obj==null || !(obj instanceof TitluCalatorieUrban)) {
			return false;
		}
		
		TitluCalatorieUrban tu = (TitluCalatorieUrban) obj;

		return tu.getId()==this.getId() && tu.getDenumire().equals(this.getDenumire()) &&
				tu.getIdLinie()==this.getIdLinie() && tu.getDataStart().equals(this.getDataStart()) &&
				tu.getDataStop().equals(this.getDataStop()) && tu.denumireOperatorUrban.equals(this.denumireOperatorUrban);
	}

	//HASHCODE
	@Override
	public int hashCode() {
		
		return (int)(31*this.getId()+31*this.getIdLinie()+31*this.getDenumire().length()+31*this.denumireOperatorUrban.length());
	}

	//TOSTRING
	@Override
	public String toString() {

		return new String("\n\nID: "+this.getId()+"\nDenumire: "+this.getDenumire()+
				"\nID Linie: "+this.getIdLinie()+"\nData Start: "+this.getDataStart()+
				"\nData Stop: "+this.getDataStop()+"\nID Zona + Denumire Operator Urban: "+this.getIdZonaAbs());
		
	}

	@Override
	public int compareTo(TitluCalatorieUrban o) {
		// TODO Auto-generated method stub
		return this.getDataStop().compareTo(o.getDataStop());
	}
	
	
	
	
	
	

	
	
	
}
