package eu.ase;

import java.io.Serializable;

public abstract class TitluCalatorie implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1043906683278865089L;
	private int id;
	private String denumire;
	private float idLinie;
	private String dataStart;
	private String dataStop;
	private final int idZona = 1; 
	
	
	//CONSTRUCTOR
	public TitluCalatorie(int id, String denumire, float idLinie, String dataStart, String dataStop) {
		this.id=id;
		this.denumire=denumire;
		this.idLinie=idLinie;
		this.dataStart=dataStart;
		this.dataStop=dataStop;
	}


	//GETTERI
	public int getId() {
		return id;
	}


	public String getDenumire() {
		return denumire;
	}


	public float getIdLinie() {
		return idLinie;
	}


	public String getDataStart() {
		return dataStart;
	}


	public String getDataStop() {
		return dataStop;
	}


	public int getIdZona() {
		return idZona;
	}
	
	
	public abstract String getIdZonaAbs();
	

}
