package eu.ase;

import java.io.Serializable;

public abstract class Pasager implements Serializable, Comparable<Pasager>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6262599825917892328L;
	private String nume;
	private float numarLoc;
	private float varsta;
	private final int idRezervare;
	
	public Pasager(String nm, float nrl, float vr, int id) {
		
		this.nume=nm;
		this.numarLoc=nrl;
		this.varsta=vr;
		this.idRezervare=id;
	}

	//GETTERS
	public String getNume() {
		return nume;
	}

	public float getNumarLoc() {
		return numarLoc;
	}

	public float getVarsta() {
		return varsta;
	}

	public int getIdRezervare() {
		return idRezervare;
	}
	
	public abstract String getIdRezevare();
	
	
	
	
	
	
}
