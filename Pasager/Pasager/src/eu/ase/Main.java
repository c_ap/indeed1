package eu.ase;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main {

	public static void main(String[] args) {

		//PASAGERVIP
		PasagerVIP pv1 = null;
		try {
			pv1 = new PasagerVIP("Ionela",1,20,1,"CelMaiBunCard");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PasagerVIP pv2 =null;
		try {
			pv2 = new PasagerVIP("Cristi",2,30,2,"CardDeCard");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		System.out.println(pv1);
		System.out.println();
		System.out.println(pv2);
		
		
		
		//AICI FACEM CU CLONE
		PasagerVIP pv3 = null;
		try {
			pv3 = (PasagerVIP)pv1.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\npv1 egal pv3: "+pv1.equals(pv3)+"\n");
		
		try {
			pv1 = new PasagerVIP("CINE VREAU EU",1,20,1,"CelMaiBunCard");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	
		
		System.out.println("pv1 egal pv3: "+pv1.equals(pv3)+'\n');
		System.out.println(pv1);
		System.out.println("\n\nClona:");
		System.out.println(pv3);
		
		
		
		
		//PASAGER EC
		PasagerEc pe1 = null;
		
		try {
			pe1=new PasagerEc("Cineva",3,40,2,"dadada");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\n\n\n\nPASAGER EC\n\n\n"+pe1);
		
		PasagerEc pe2 = null;
		
		try {
			pe2=(PasagerEc)pe1.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\n\n\np31 egal pv2: "+pe1.equals(pe2)+'\n');
		
		try {
			pe1=new PasagerEc("ALTCineva",3,40,2,"dadada");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\n\n\np31 egal pv2: "+pe1.equals(pe2)+'\n');
		
		
		
		//MATRICE
		Object [][]obs= {{pv2,pv3},{pe1,pe2}};
		Matrice m = new Matrice(2,2,obs);
		
		System.out.println("\n\n\n\n\nMatrice\n\n");
		m.printMatrice();
		
		Matrice m1=null;
		try {
			m1 = (Matrice)m.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//m.setObiecte(new Object[][]{{pv2},{pe1}});
		
		
		
		System.out.println("\n\nMatrice egale: "+m.equals(m1));
		
		m.printMatrice();
		
		
		
		//CITIRE MATRICE DIN FISIER
		
		Matrice mat=new Matrice();
		Object [][] matrix;
		mat.setLinii(4);
		mat.setColoane(8);
		matrix=new Object[4][8];
		mat.setObiecte(matrix);
		mat.citireFisier("PasagerEc.txt");
		mat.citireFisier("PasagerVip.txt");
		
		//mat.printMatrice();
		
		
		//SERIALIZARE
		
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("da.dat"));
			out.writeObject(mat);
			out.close();
			
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("da.dat"));
			Matrice ma =(Matrice)in.readObject();
			in.close();
			//ma.printMatrice();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		mat.sortMatrice();
		mat.printMatrice();
		
	}

}
