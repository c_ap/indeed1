package eu.ase;

public class PasagerEc extends Pasager implements Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6476777027776536359L;
	private String nrCarEc;
	
	
	public PasagerEc(String nm, float nrl, float vr, int id, String card ) throws Exception {
		super(nm, nrl, vr, id);
		if(nrl<0) {
			throw new Exception();
		}
		this.nrCarEc=card;
	}


	@Override
	public String getIdRezevare() {
		// TODO Auto-generated method stub
		return new String(this.getIdRezervare()+this.nrCarEc);
	}


	@Override
	protected Object clone() throws CloneNotSupportedException {

		PasagerEc pe = (PasagerEc)super.clone();
		
		try {
			return new PasagerEc(pe.getNume(), pe.getNumarLoc(), pe.getVarsta(), pe.getIdRezervare(), pe.nrCarEc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}


	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(obj == null || !(obj instanceof PasagerEc)) {
			return false;
		}
		PasagerEc pe = (PasagerEc)obj;
		return this.getNume().equals(pe.getNume()) &&
				this.getNumarLoc()==pe.getNumarLoc() &&
				this.getVarsta()==pe.getVarsta() &&
				this.nrCarEc.equals(pe.nrCarEc) &&
				this.getIdRezervare()==pe.getIdRezervare();
	}


	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		 return (int)(31*this.getNume().length()+
				31*this.getNumarLoc()+ 31*this.getVarsta()+
				31*this.nrCarEc.length()+31*this.getIdRezervare());
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return new String("Nume: "+this.getNume()+"\nNumar loc: "+this.getNumarLoc()+
				"\nVarsta: "+this.getVarsta()+"\nId-Card: "+this.getIdRezevare());
	}


	@Override
	public int compareTo(Pasager o) {
	
		return Integer.compare(this.getIdRezervare(), o.getIdRezervare());
	}
	
	
	
	
	
}
